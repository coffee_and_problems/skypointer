#! /usr/bin/env python

from math import copysign
from astropy.coordinates import SkyCoord
from astropy.coordinates import FK5
from astropy.time import Time
from astropy import units
from astropy.coordinates import EarthLocation
from .suppl_funcs import get_decimal_year
from astropy.utils import iers
iers.conf.auto_download = False


class Aligner(object):
    def __init__(self):
        # load align stars
        self.align_stars = []
        path_to_stars = "data/align_stars.dat"
        for line in open(path_to_stars):
            if line.startswith("#"):
                continue
            params = line.split()
            name = params[0]
            sao_number = int(params[1])
            magnitude = float(params[2])
            rah, ram, ras = [float(p) for p in params[3].split(":")]
            decd, decm, decs = [float(p) for p in params[4].split(":")]
            ra_degrees = 15 * (rah + ram/60 + ras / 3600)
            dec_degrees = copysign(abs(decd) + decm / 60 + decs / 3600, decd)
            catalog_coords = SkyCoord(ra=ra_degrees, dec=dec_degrees, frame=FK5(equinox="J2000"), unit="deg")
            equatorial_coords = catalog_coords.transform_to(frame=FK5(equinox="J%1.2f" % get_decimal_year()))

            star = {"name": name, "sao_number": sao_number, "magnitude": magnitude,
                    "rah": rah, "ram": ram, "ras": ras, "decd": decd, "decm": decm, "decs": decs,
                    "equatorial": equatorial_coords}

            self.align_stars.append(star)

    def find_best_star(self, location):
        """
        Finds the best alignment star based on the current time. The best star is one
        closer to the culmination.
        """
        curent_time = Time(Time.now(), location=location)
        curent_time.delta_ut1_utc = 0.
        st = curent_time.sidereal_time("apparent")
        t_min = 12  # time between the culmination ant the current time
        for star in self.align_stars:
            hour_angle = st.hour-star["equatorial"].ra.hour
            if abs(hour_angle) < t_min:
                t_min = abs(hour_angle)
                best_star = star
        self.st = st.hour
        return best_star
