![](data/image.png)

A tool to control LX200 telescope movements

## Installation

You need to install the dbm gnu module for your operating system. On Ubuntu LTS, you can try:

```bash
sudo apt install python3.6-gdbm
```

If you are using a virtual environment, you must create it with access to the system packages.

```bash
python3.6 -m venv --system-site-packages venv
source venv/bin/activate
(venv)
```

To install the dependencies required use:

```bash
(venv) pip install -r requirements.txt
```

And then you can finally run `skypointer.py`.
